// Import thư viện mongoose
const mongoose = require("mongoose");

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const courseSchema = new Schema({
    title: {
        type: String,
        required: false,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    reviews: [{
        type: mongoose.Types.ObjectId,
        ref: "Review"
    }],
    // Trường hợp 1 course có 1 reivew
    // review: {
    //     type: mongoose.Types.ObjectId,
    //     ref: "Review"
    // }
}, {
    timestamps: true
});

module.exports = mongoose.model("Course", courseSchema);