// Import thư viện mongoose 
const mongoose = require("mongoose");

// Khai báo class Schema 
const Schema = mongoose.Schema;

// Khởi tạo 1 instance reviewSchema từ Class Schema
const reviewSchema = new Schema({
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    }
}, {
    timestamps: true
}); 

module.exports = mongoose.model("Review", reviewSchema);